package kos.cpgmn.calculator.domain.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DomainServiceImpTest {

	DomainServiceImp domainServiceImp;
	
	@BeforeEach
	void setUp() {
		domainServiceImp = new DomainServiceImp();
	}
	
	@Test
	void validateCorrelativeSequenceThrowsAnErrorWhenTheInputListSequenceIsNull() {
 		
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(null));		
	}
	
	@Test
	void validateCorrelativeSequenceThrowsAnErrorWhenTheInputListSequenceIsEmpty() {

		List<Integer> sequence = new ArrayList<Integer>();
		
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(sequence));	
	}
	
	@Test
	void exceptionShouldBeThrownWhenInTheInputSequenceSomeDuplicatedNumbersAreFound() {
		
		List<Integer> sequence = Arrays.asList(1,2,2,2,5);
		
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(sequence));	
	}

	@Test
	void exceptionShouldBeThrownWhenTheInputSequenceIsNotASequenceWithAMissingNumber() {
		
		List<Integer> noCorrelativeSequence = Arrays.asList(1,333,18,666,5);
		List<Integer> correlativeSequenceWithoutMissingNumber = Arrays.asList(1,2,3,4,5);
		
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(noCorrelativeSequence));
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp
				.validateCorrelativeSequenceWithMissingNumber(correlativeSequenceWithoutMissingNumber));
	
	}
	
	@Test
	void exceptionIsNotThrownWhenTheSequenceSizeIsASequenceWithAMissingNumber() {	
		
		List<Integer> orderedSequence = Arrays.asList(1,2,4,5);
		List<Integer> unorderedSequence = Arrays.asList(1,5,4,2);
		
		assertDoesNotThrow( () -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(orderedSequence));
		assertDoesNotThrow( () -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(unorderedSequence));
	}
	
	@Test
	void findingTheMissingNumberWhenTheSequenceIsNotValidatedAnExceptionIsThrown() {
	
		List<Integer> emptySequence = new ArrayList<Integer>();
		List<Integer> nullSequence = null;
		List<Integer> notACorrelativeSequence = Arrays.asList(1,333,18,666,5);
		List<Integer> correlativeSequenceWithoutMissingNumber = Arrays.asList(1,2,3,4,5);
		List<Integer> duplicatedValuesSequence = Arrays.asList(1,2,2,2,5);
		
		assertAll(
		()-> assertThrows(IllegalArgumentException.class, () -> domainServiceImp.findTheMissingNumberInASequence(emptySequence)),
		()-> assertThrows(IllegalArgumentException.class, () -> domainServiceImp.findTheMissingNumberInASequence(nullSequence )),
		()-> assertThrows(IllegalArgumentException.class, () -> domainServiceImp.findTheMissingNumberInASequence(notACorrelativeSequence)),
		()-> assertThrows(IllegalArgumentException.class, () -> domainServiceImp.findTheMissingNumberInASequence(correlativeSequenceWithoutMissingNumber )),
		()-> assertThrows(IllegalArgumentException.class, () -> domainServiceImp.findTheMissingNumberInASequence( duplicatedValuesSequence))
		);
	}
	
	@Test
	void findingTheMissingNumberWhenTheOrderedOrUnorderedSequenceIsValidDoesNotThrowAnException() {
		
		List<Integer> orderedSequence = Arrays.asList(1,2,4,5);
		List<Integer> unorderedSequence = Arrays.asList(1,5,4,2);
		
		assertDoesNotThrow( () -> domainServiceImp.findTheMissingNumberInASequence(orderedSequence));
		assertDoesNotThrow( () -> domainServiceImp.findTheMissingNumberInASequence(unorderedSequence));
	}
	
	@Test
	void findingTheMissingNumberWhenTheOrderedOrUnorderedSequenceGetsTheMissingNumber() {
		
		Integer missedNumber = 666;
		List<Integer> orderedSequence = Arrays.asList(664,665,667,668);
		List<Integer> unorderedSequence = Arrays.asList(667,665,668,664);
		
		assertEquals(missedNumber, domainServiceImp.findTheMissingNumberInASequence(orderedSequence));
		assertEquals(missedNumber, domainServiceImp.findTheMissingNumberInASequence(unorderedSequence));	
	}
	
	@Test
	void findingSomeNumberInTheSequenceMinorOrEqualToZeroAnExceptionIsThrown() {
		
		List<Integer> sequenceWithNegativeNumber = Arrays.asList(-1,1,2,3,4);
		List<Integer> sequenceWithZero = Arrays.asList(0,1,2,3,5);
		
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(sequenceWithNegativeNumber));
		assertThrows(IllegalArgumentException.class,
				() -> domainServiceImp.validateCorrelativeSequenceWithMissingNumber(sequenceWithZero));
	}
}
