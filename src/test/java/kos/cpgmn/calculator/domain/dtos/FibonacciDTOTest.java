package kos.cpgmn.calculator.domain.dtos;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class FibonacciDTOTest {

	@Test
	void aFibonacciDTOShouldBeCreatedSuccessfullyWhenMissingNumberAndSequenceAreProvidedToTheConstructor() {
		Integer numberMissed = 5;	
		List<Integer> sequence = Arrays.asList( 1,2,3,4,6,7);
		
		FibonacciDTO theFibonacci = FibonacciDTO.builder()
														.missingNumber(numberMissed)
														.fibonacciSequence(sequence)
														.build();
		
		assertDoesNotThrow( () -> new FibonacciDTO(null, numberMissed, sequence));
		assertEquals(5, theFibonacci.missingNumber());
		assertEquals(Arrays.asList( 1,2,3,4,6,7), theFibonacci.fibonacciSequence());
	}
	
	@Test
	void aFibonacciDTOShouldThrowAnIllegalArgumentExceptionWhenTheMissingNumberIsNotSpecified() {
		List<Integer> sequence = Arrays.asList( 1,2,3,4,6,7);
		
		assertThrows(IllegalArgumentException.class, () -> new FibonacciDTO(null, null, sequence));
		
	}
	
	@Test
	void aFibonacciDTOShouldThrowAnIllegalArgumentExceptionWhenTheSequenceIsNotSpecified() {
		Integer numberMissed = 5;	
		
		assertThrows(IllegalArgumentException.class, () -> new FibonacciDTO(null, numberMissed, null));
		
	}

}
