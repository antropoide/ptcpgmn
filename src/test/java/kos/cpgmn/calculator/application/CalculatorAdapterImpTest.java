package kos.cpgmn.calculator.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import kos.cpgmn.calculator.CalculatorApplicationTests;
import kos.cpgmn.calculator.application.dto.MissingNumberDTO;
import kos.cpgmn.calculator.domain.applications.CalculatorApplicationService;

/**
 * This testing class extend the ApplicationTests class in order to allow 
 *  the domain service classes mocking , which are declared as @Beans
 *   in @Configuration classes. (CalculatorApplicationService  p.e.)
 *   So, the Spring ApplicationContext is needed, and as it is needed, by extending
 *   the ApplicationTests class the application context is shared, which is more efficient than fire up the context 
 *   by each test.
 */
class CalculatorAdapterImpTest extends  CalculatorApplicationTests {
	@Mock
	private CalculatorApplicationService calculatorApplicationService;
	
	@InjectMocks
	private CalculatorAdapterImp calculatorAdapterImp;
	
	@Test
	void theMissingNumberIsFoundWhenSequenceIsValid() {
		
        List<Integer> inputSequence = Arrays.asList(1, 2, 3, 5);
        Integer expectedMissingNumber = 4;
        when(calculatorApplicationService.calculateMissingNumber(anyList())).thenReturn(expectedMissingNumber);

        ResponseEntity<MissingNumberDTO> response = calculatorAdapterImp.getTheMissingNumber(inputSequence);

        assertEquals(expectedMissingNumber, response.getBody().missingNumber());
        assertEquals(HttpStatus.OK, response.getStatusCode());
		
	}
	
    @Test
    void anIllegalArgumentExceptionIsGotWhenTheListHasNotAMissingNumber() {
        List<Integer> inputSequence = Arrays.asList(1, 2, 3, 4,5);
        when(calculatorApplicationService.calculateMissingNumber(anyList())).thenThrow(IllegalArgumentException.class);

        ResponseEntity<MissingNumberDTO> response = calculatorAdapterImp.getTheMissingNumber(inputSequence);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());   
      }
    
    @Test
    void anIllegalArgumentExceptionIsGotWhenTheListIsNotASequenceWithAMissingNumberInside() {
        List<Integer> inputSequence = Arrays.asList(114, 2, 333, 14,54);
        when(calculatorApplicationService.calculateMissingNumber(anyList())).thenThrow(IllegalArgumentException.class);

        ResponseEntity<MissingNumberDTO> response = calculatorAdapterImp.getTheMissingNumber(inputSequence);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());   
      }
    
    @Test
    void anIllegalArgumentExceptionIsGotWhenTheSequenceIsNull() {

        when(calculatorApplicationService.calculateMissingNumber(null)).thenThrow(IllegalArgumentException.class);

        ResponseEntity<MissingNumberDTO> response = calculatorAdapterImp.getTheMissingNumber(null);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());   
      }
    
    @Test
    void anIllegalArgumentExceptionIsGotWhenTheSequenceIsEmpty() {
    	List<Integer> inputSequence = new ArrayList<Integer>();
    	
        when(calculatorApplicationService.calculateMissingNumber(anyList())).thenThrow(IllegalArgumentException.class);

        ResponseEntity<MissingNumberDTO> response = calculatorAdapterImp.getTheMissingNumber(inputSequence );

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());   
      }

}
