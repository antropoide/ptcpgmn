package kos.cpgmn.calculator.domain.ports;

import java.util.List;

import kos.cpgmn.calculator.domain.dtos.FibonacciDTO;

/**
 *  Port persistence interface which handle all the operations needed by the core over the H2 DB
 */
public interface PersistenceH2DBPort {
	
	/**
	 * Counts all the records in the FibonacciHistoric table.
	 * 
	 * @return Integer
	 */
	Integer count();
	
	/**
	 * Saves a {@link FibonacciDTO Fibonacci} sequence object in DB
	 * 
	 * @param  fibonacciDTO FibonacciDTO
	 * @return FibonacciDTO
	 */
	FibonacciDTO saveFibonacciSequence(FibonacciDTO fibonacciDTO);
	
	/**
	 * Gets all the calculated Fibonacci sequences.
	 * 
	 * @return List FibonacciDTO
	 */
	List<FibonacciDTO> getAllFibonacciSequencesFromHistoric();
	
	
	
}
