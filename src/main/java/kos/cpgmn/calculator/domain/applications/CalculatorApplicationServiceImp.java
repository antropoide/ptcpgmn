package kos.cpgmn.calculator.domain.applications;

import java.util.List;


import kos.cpgmn.calculator.domain.dtos.FibonacciDTO;
import kos.cpgmn.calculator.domain.service.DomainService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CalculatorApplicationServiceImp implements CalculatorApplicationService {
	
	private final DomainService domainService;

	@Override
	public Integer calculateMissingNumber(List<Integer> inputList) {

			// return the calculated missing number
		return domainService.findTheMissingNumberInASequence(inputList);
	}

	@Override
	public FibonacciDTO calculateFibonacciSequence(Integer size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FibonacciDTO> getTheSequencesHistory(Integer calculationsNumber) {
		// TODO Auto-generated method stub
		return null;
	}

}
