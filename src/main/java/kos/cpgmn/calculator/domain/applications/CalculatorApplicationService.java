package kos.cpgmn.calculator.domain.applications;

import java.util.List;

import kos.cpgmn.calculator.domain.dtos.FibonacciDTO;

/**
 *  Interface which establishes the core functional contract for the Calculation application layer.
 */
public interface CalculatorApplicationService {
	
	/**
	 * Given an input list, the method will return the missing number in that sequence.
	 * Example: For the sequence: 1,2,3,5 ; the missing number is 4.
	 * 
	 * @param inputList List<Integer> The correlative sequence with a missing number.
	 * @return Integer The missing number.
	 */
	Integer calculateMissingNumber(List<Integer> inputList);
	
	/**
	 *  Calculate the Fibonacci sequence of size N.
	 *  The Fibonacci list will be returned as unsorted list and must have a missing number.
	 *	 The missing number will be calculated randomly.
	 *  The sequence and the missing number will be saved in DB as a  {@link FibonacciDTO}
	 *
	 * @param size Integer
	 * @return FibonacciDTO
	 */
	FibonacciDTO calculateFibonacciSequence(Integer size);
	
	/**
	 * List the <b>last N </b> Fibonacci sequence calculations made.
	 * 
	 * @param calculationsNumber Integer The desired number of calculations
	 * @return List<FibonacciDTO> The calculationsNumber last calculations.
	 */
	List<FibonacciDTO> getTheSequencesHistory(Integer calculationsNumber);
	
	
}
