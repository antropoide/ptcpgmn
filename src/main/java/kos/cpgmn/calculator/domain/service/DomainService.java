package kos.cpgmn.calculator.domain.service;

import java.util.List;

/**
 * Utility service class for the different domain application requirements.
 */
public interface DomainService {
	
	/**
	 *  It verifies the integer input list is a sequence with a missing number between the min and max values. 
	 * 
	 * @param sequence List<Integer> The sequence with a missing number.
	 * @throw IllegalArgumentException if the input list is not validated.
	 */
	void validateCorrelativeSequenceWithMissingNumber(List<Integer> sequence);

	/**
	 * Find the missing number inside of a correlative sequence.
	 * 
	 * @param sequence List<Integer> The sequence with a missing number.
	 * @return Integer The missing number.
	 * @throw IllegalArgumentException if the input list is not validated.
	 */
	Integer findTheMissingNumberInASequence(List<Integer> sequence);

}
