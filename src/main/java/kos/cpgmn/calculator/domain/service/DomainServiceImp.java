package kos.cpgmn.calculator.domain.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import lombok.RequiredArgsConstructor;

/**
 * Utility service class  implementation for the different domain application requirements.
 */

@RequiredArgsConstructor
public class DomainServiceImp implements DomainService {

	@Override
	public void validateCorrelativeSequenceWithMissingNumber(List<Integer> sequence) {
		
        validateSequenceNotNullOrEmpty(sequence);
        validateNoDuplicatedValuesInASequence(sequence);
        validateSequenceBySizeAndEdgeValues(sequence);
        validateNumbersInSequenceAreGreaterThanZero(sequence);
	}
	
	private void validateNumbersInSequenceAreGreaterThanZero(List<Integer> sequence) {
		
		if(sequence.stream().anyMatch(n -> n<=0 )){
			throw new IllegalArgumentException("There are some numbers equal or minor than zero in the sequence: " 
									+ sequence.toString());
		}
		
	}

	@Override
	public Integer findTheMissingNumberInASequence(List<Integer> sequence) {
		
		validateCorrelativeSequenceWithMissingNumber(sequence);

        Collections.sort(sequence);

        // Iterate through the sequence to find the gap
        for (int i = 0; i < sequence.size() - 1; i++) {
            // If the difference between the current number and the next number is more than 1,
            // then there is a missing number
            if (sequence.get(i + 1) - sequence.get(i) > 1) {
                // Return the missing number, which is the current number plus 1
                return sequence.get(i) + 1;
            }
        }
        // If no missing number is found, throw an exception or return a specific value. 
        // After validation this exception should never be reached. Just for guarantee the right method behavior.
        throw new IllegalArgumentException("No missing number found in the sequence.");
	}

	private void validateSequenceBySizeAndEdgeValues(List<Integer> sequence) {
		
		Collections.sort(sequence);
        int min = sequence.get(0);
        int max = sequence.get(sequence.size() - 1);

        if( max - min != sequence.size()) {
        	throw new IllegalArgumentException("The input sequence is not a correlative sequence with a missing number inside: " 
        			+ sequence.toString());       	   	
        }
	}

	private void validateNoDuplicatedValuesInASequence(List<Integer> sequence) {
		// Convert the list to a set to remove duplicates
        Set<Integer> uniqueSequence = new HashSet<>(sequence);
        if (uniqueSequence.size() < sequence.size()) {
        	throw new IllegalArgumentException("Repeated values found in the input sequence: " 
        			+ sequence.toString());
        }
	}

	private void validateSequenceNotNullOrEmpty(List<Integer> sequence) {
		if (Objects.isNull(sequence) ) {
            throw new IllegalArgumentException("The input sequence is null.");
        } else if (sequence.isEmpty()) {
        	throw new IllegalArgumentException("The input sequence is empty: " + sequence.toString());
        }
	}
}
