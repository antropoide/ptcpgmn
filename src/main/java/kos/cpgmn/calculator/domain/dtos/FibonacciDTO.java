package kos.cpgmn.calculator.domain.dtos;

import java.util.List;
import java.util.Objects;

import lombok.Builder;

/**
 * Represents a Fibonacci sequence object in the domain.
 * 
 */
@Builder(toBuilder = true)
public record FibonacciDTO(Long order, Integer missingNumber, List<Integer> fibonacciSequence) {

	/**
	 * FibonacciDTO constructor.
	 * 
	 * @param order Long, as Id in FibonacciHistoric table.
	 * @param missingNumber Integer, NonNull, as random missing number from the original sequence.
	 * @param fibonacciSequence List<Integer>, NonNull, as the Fibonacci sequence except the missingNumber.
	 */
	public FibonacciDTO {	
		if(Objects.isNull(missingNumber)) {
			throw new IllegalArgumentException();
		}		
		if(Objects.isNull(fibonacciSequence)) {
			throw new IllegalArgumentException();
		}
	}
}
