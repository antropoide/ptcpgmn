package kos.cpgmn.calculator.infraestructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import kos.cpgmn.calculator.domain.service.DomainService;
import kos.cpgmn.calculator.domain.service.DomainServiceImp;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class DomainBeansFactory {

	@Bean
	DomainService domainService() {
		return new DomainServiceImp();
	}
}
