package kos.cpgmn.calculator.infraestructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import kos.cpgmn.calculator.domain.applications.CalculatorApplicationService;
import kos.cpgmn.calculator.domain.applications.CalculatorApplicationServiceImp;
import kos.cpgmn.calculator.domain.service.DomainService;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class DomainBeansServiceFactory {

	private final DomainService domainService;
	
	@Bean
	CalculatorApplicationService calculatorApplicationServiceImp() {
		return new CalculatorApplicationServiceImp(domainService);
	}
}
