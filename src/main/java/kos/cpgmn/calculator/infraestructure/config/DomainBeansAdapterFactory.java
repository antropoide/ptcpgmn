package kos.cpgmn.calculator.infraestructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import kos.cpgmn.calculator.application.CalculatorAdapter;
import kos.cpgmn.calculator.application.CalculatorAdapterImp;
import kos.cpgmn.calculator.domain.applications.CalculatorApplicationService;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class DomainBeansAdapterFactory {

	private final CalculatorApplicationService calculatorApplicationService;
	
	@Bean
	CalculatorAdapter calculatorAdapterImp() {
		return new CalculatorAdapterImp(calculatorApplicationService);
	}
}
