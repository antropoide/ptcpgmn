package kos.cpgmn.calculator.application;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kos.cpgmn.calculator.application.dto.MissingNumberDTO;
import lombok.RequiredArgsConstructor;

/**
 *  Contains  the API endpoints for calculations as specified in requirements. 
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/") 
public class CalculatorController {
	
	private final CalculatorAdapter calculatorAdapter;
	
	@GetMapping("calculate")
	ResponseEntity<MissingNumberDTO> getTheMissingNumberInASequence(
			@RequestParam List<Integer> sequence){
		return calculatorAdapter.getTheMissingNumber(sequence);
	}
	
	

}
