package kos.cpgmn.calculator.application;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import kos.cpgmn.calculator.application.dto.MissingNumberDTO;
import kos.cpgmn.calculator.domain.applications.CalculatorApplicationService;
import kos.cpgmn.calculator.domain.dtos.FibonacciDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class CalculatorAdapterImp implements CalculatorAdapter{
	
	private final CalculatorApplicationService calculatorApplicationService;

	@Override
	public ResponseEntity<MissingNumberDTO> getTheMissingNumber(List<Integer> inputSequence) {

		Integer missingNumber= null;
		try {
			missingNumber = calculatorApplicationService.calculateMissingNumber(inputSequence);
		} catch (IllegalArgumentException e) {
			log.atError().log(e.getMessage());
			return handleIllegalArgumentExceptionForMissingNumber(e);
		}
		
		MissingNumberDTO theMissingNumberToReturn = new MissingNumberDTO(missingNumber);
		
		return ResponseEntity.ok().body(theMissingNumberToReturn);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	private ResponseEntity<MissingNumberDTO> handleIllegalArgumentExceptionForMissingNumber(IllegalArgumentException e) {
		return ResponseEntity.badRequest().build();
	}

	@Override
	public ResponseEntity<FibonacciDTO> getAndSaveTheFibonacciSequence(Integer size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<FibonacciDTO>> getTheSequencesHistory(Integer calculationsNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
