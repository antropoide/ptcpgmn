package kos.cpgmn.calculator.application.dto;

/**
 * Contains the missing number in a sequence state.
 */
public record MissingNumberDTO(Integer missingNumber) {

}
