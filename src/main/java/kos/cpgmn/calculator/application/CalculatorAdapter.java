package kos.cpgmn.calculator.application;

import java.util.List;

import org.springframework.http.ResponseEntity;

import kos.cpgmn.calculator.application.dto.MissingNumberDTO;
import kos.cpgmn.calculator.domain.dtos.FibonacciDTO;

/**
 *  Describes the functionality contract available to the controllers.
 */
public interface CalculatorAdapter {

	/**
	 * Given an input list, the method will return the missing number in that sequence.
	 * Example: For the sequence: 1,2,3,5 ; the missing number is 4.
	 * 
	 * @param inputList List<Integer> The correlative sequence with a missing number.
	 * @return ResponseEntity<MissingNumberDTO> The missing number.
	 */
	ResponseEntity<MissingNumberDTO> getTheMissingNumber(List<Integer> inputSequence);
	
	/**
	 *  Calculate the Fibonacci sequence of size N.
	 *  The Fibonacci list will be returned as unsorted list and must have a missing number.
	 *	 The missing number will be calculated randomly.
	 *  The sequence and the missing number will be saved in DB as a  {@link FibonacciDTO}
	 *
	 * @param size Integer
	 * @return ResponseEntity<FibonacciDTO>
	 */
	ResponseEntity<FibonacciDTO> getAndSaveTheFibonacciSequence(Integer size);
	
	/**
	 * List the <b>last N </b> Fibonacci sequence calculations made.
	 * 
	 * @param calculationsNumber Integer The desired number of calculations
	 * @return ResponseEntity<List<FibonacciDTO>> The calculationsNumber last calculations.
	 */
	ResponseEntity<List<FibonacciDTO>> getTheSequencesHistory(Integer calculationsNumber);
	
}
