
# Table of Contents

1.  [/api/v1/calculate](#org4f83f4a)
2.  [/api/v1/calculate/ﬁbonacci?size=n](#org36ee258)
3.  [/api/v1/calculate/historic?calcula8ons=n](#org0477fd7)

**API CALCULATOR**
Develops a Rest API to execute diﬀerent numerical algorithms calculation.


<a id="org4f83f4a"></a>

# /api/v1/calculate

-   Ok

curl -v <http://localhost:8080/api/v1/calculate?sequence=1,2,3,5>

-   OK. Unordered list

curl -v <http://localhost:8080/api/v1/calculate?sequence=667,664,668,665>

-   KO. Sequenced list without missing number.

curl -v <http://localhost:8080/api/v1/calculate?sequence=1,2,3,4,5>

-   KO. List is not a sequence.

curl -v <http://localhost:8080/api/v1/calculate?sequence=16,233,3,13454,555>

-   KO. List is not a sequence.

curl -v <http://localhost:8080/api/v1/calculate?sequence=16>

-   KO. List is empty

curl -v <http://localhost:8080/api/v1/calculate?sequence>=

-   KO. List is null

curl -v <http://localhost:8080/api/v1/calculate?sequence> 

-   KO. List has letters

curl -v <http://localhost:8080/api/v1/calculate?sequence=12,f,45,6> 

-   KO. List has negative numbers

curl -v <http://localhost:8080/api/v1/calculate?sequence=-1,1,2,3,4>

-   KO. List has the zero number.

curl -v <http://localhost:8080/api/v1/calculate?sequence=0,2,3,4>


<a id="org36ee258"></a>

# /api/v1/calculate/ﬁbonacci?size=n

-   Work in progress.


<a id="org0477fd7"></a>

# /api/v1/calculate/historic?calcula8ons=n

-   Work in progress.

